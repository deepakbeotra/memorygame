package com.example.deepak.cardflip;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import java.util.ArrayList;

/**
 * Created by deepak on 10/16/15.
 *
 * Class to handle Image View Objects
 */
public class ImageAdapter extends BaseAdapter {

    private ArrayList<ImageView> imageViews = new ArrayList<>();
    public ImageAdapter(ArrayList<ImageView> imageViews) {
        this.imageViews = imageViews;
    }

    @Override
    public int getCount() {
        return imageViews.size();
    }

    @Override
    public Object getItem(int position) {
        return imageViews.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = imageViews.get(position);
        return convertView;
    }
}
