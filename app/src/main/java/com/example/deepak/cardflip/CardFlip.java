package com.example.deepak.cardflip;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.res.AssetManager;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * Class which renders the objects of imageviews(card_layout.xml) on main layout and initialise the game.
 */
public class CardFlip extends AppCompatActivity {
    private static final String TAG = "CardFlip Activity";

    // variable to store num of cards
    private static int numOfCards = 20;
    // this variable depicts the position of previous card. i.e. if there is opened card, then this variable
    // stores the position of that card otherwise -1;
    private int prevOpendCardPos = -1;
    // variable to store count of correct guesses
    private int correctGuessCount = 0;
    // variable to store count of wrong guesses.
    private int wrongGuessCount = 0;
    // textview to show correctguesses.
    private TextView correctGuessCountTextView;
    // textview to show wrongguesses.
    private TextView wrongGuessCountTextView;
    // arraylist to store images
    private ArrayList<Drawable> images = new ArrayList<>();
    // wwe logo string
    private static final String WWE_LOGO = "wwe_logo.png";

    private Drawable wwe_logo_image;
    private GridView gridView;
  //  arraylist to store names of images
    private ArrayList<String> imageNameList;
    // araylist to store imageviews
    private ArrayList<ImageView> imageViews;
    private ImageAdapter imageAdapter;
    private Animation moveInAnimation; // animation to move in
    private Animation moveOutAnimation; // animation for move out
    private Animation shakeAnimation;
    private ImageView newOpendCard;
    private ImageView prevOpendCard;
    private LinearLayout linearLayout;

    private static int round = 1;
    private boolean found = false;
    // create constants for each menu id
    private static final int NEW_GAME = Menu.FIRST;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_flip);
        setViews();
        createAnimations();
        imageNameList = new ArrayList<>();
        imageViews = new ArrayList<ImageView>();
        wwe_logo_image = getImageFromAssets(WWE_LOGO);
        setWWELogoCards();
        Toast.makeText(this,"Welcome to New Game", Toast.LENGTH_SHORT).show();
    }

    /**
     *  this method inflates the card layouts on the game and sets listener to each card
     *
     * this method also sets the image adapter
     */
    private void setWWELogoCards() {
        for(int i=0;i<numOfCards;i++){
            LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.card_layout,null);
            view.setMinimumWidth(200);
            view.setMinimumHeight(300);
            ImageView imageView = (ImageView) view;
            imageView.setImageDrawable(wwe_logo_image);
            imageView.setOnClickListener(imageClickListener);
            imageViews.add(imageView);
        }
        setImageLists();
        imageAdapter = new ImageAdapter(imageViews);
        gridView.setAdapter(imageAdapter);
    }

    // initializing all the views
    private void setViews() {
        gridView = (GridView) findViewById(R.id.cardsGridView);
        correctGuessCountTextView = (TextView) findViewById(R.id.correctGuessCountTextView);
        wrongGuessCountTextView = (TextView) findViewById(R.id.wrongGuessCountTextView);

        // linear layout to hide the imageviews during animations
        // when linearLayout.setClickable(false), it unhides the imageviews
        // otherwise hide
        linearLayout = (LinearLayout) findViewById(R.id.linearLayout);
        linearLayout.setClickable(false);
        linearLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (linearLayout.isClickable()) {
                    return true;
                }
                return false;
            }
        });
    }

    /**
     * creating animations
     */
    private void createAnimations(){
        moveOutAnimation = AnimationUtils.loadAnimation(this, R.anim.move_out);
        moveInAnimation = AnimationUtils.loadAnimation(this, R.anim.move_in);
        shakeAnimation = AnimationUtils.loadAnimation(this, R.anim.shake);

        // setting move in animation
        moveInAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                linearLayout.setClickable(true);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (round == 1) {
                    newOpendCard.clearAnimation();
                    newOpendCard.setAnimation(moveOutAnimation);
                    newOpendCard.startAnimation(moveOutAnimation);
                } else { // when round =2
                    prevOpendCard.clearAnimation();
                    prevOpendCard.setAnimation(moveOutAnimation);
                    prevOpendCard.startAnimation(moveOutAnimation);
                    prevOpendCard.setImageDrawable(wwe_logo_image);
                    newOpendCard.setImageDrawable(wwe_logo_image);
                    newOpendCard.clearAnimation();
                    newOpendCard.setAnimation(moveOutAnimation);
                    newOpendCard.startAnimation(moveOutAnimation);
                    prevOpendCard = null;
                    round = 1;
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        // setting move out animation
        moveOutAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (prevOpendCard != null) {
                    newOpendCard.clearAnimation();
                    newOpendCard.setAnimation(moveInAnimation);
                    newOpendCard.startAnimation(moveInAnimation);

                    if (found) {
                        newOpendCard.clearAnimation();
                        newOpendCard.setAnimation(shakeAnimation);
                        newOpendCard.startAnimation(shakeAnimation);
                        prevOpendCard.clearAnimation();
                        prevOpendCard.setAnimation(shakeAnimation);
                        prevOpendCard.startAnimation(shakeAnimation);
                        prevOpendCard = null;
                        found = false;
                        linearLayout.setClickable(false);
                    } else { // when found == false
                        round = 2;
                        prevOpendCard.clearAnimation();
                        prevOpendCard.setAnimation(moveInAnimation);
                        prevOpendCard.startAnimation(moveInAnimation);
//                    } else { // when round = 2;
//
//                        prevOpendCard = null;
//                        round = 1;
//                        linearLayout.setClickable(false);
                    }
                }
                linearLayout.setClickable(false);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    /**
     * This method returns the Drawable object
     *
     * @param imageName
     * @return Drawable
     */
    private Drawable getImageFromAssets(String imageName) {
        AssetManager assets = getAssets(); // get app's AssetManager
        InputStream stream = null; // used to read in sign images

        try {
            stream = assets.open(imageName);
        } // end try
        catch (IOException e) {
            Log.e(TAG, "Error loading " + imageName, e);
        }
        return Drawable.createFromStream(stream, "");
    }

    /**
     * This method fills the imageNameLists to store the names of the images
     */
    private void setImageLists() {
        AssetManager assets = getAssets(); // get the app's AssetManager
        imageNameList.clear();
        try {
            String[] paths = assets.list("");
            for (String path : paths) {
                if (path.contains(".png") && !path.contains(WWE_LOGO)) {
                    imageNameList.add(path);
                }
            }
        }catch (IOException e) {

        }
        //shuffling the list
        Collections.shuffle(imageNameList);
        while(imageNameList.size()>numOfCards/2) {
            imageNameList.remove(0);
        }
        int index=0;
        while(imageNameList.size() != numOfCards) {
            imageNameList.add(imageNameList.get(index));
            index++;
        }
        Collections.shuffle(imageNameList);
        setImages(imageNameList);
    }

    /**
     *This method sets the images on imageviews
     * @param imageNameList
     */
    private void setImages(ArrayList<String> imageNameList) {
        images.clear();
        AssetManager assets = getAssets(); // get app's AssetManager
        InputStream stream = null; // used to read in sign images
        for(String imageName : imageNameList) {
            try {
                stream = assets.open(imageName);

            } // end try
            catch (IOException e) {
                Log.e(TAG, "Error loading " + imageName, e);
            }

            // load the asset as a Drawable and display on the signImageView
            images.add(Drawable.createFromStream(stream, ""));
        }
    }

    private View.OnClickListener imageClickListener = new View.OnClickListener() {
        @Override
        public void onClick(final View v) {
            newOpendCard = (ImageView) v;
            newOpendCard.clearAnimation();
            newOpendCard.setAnimation(moveInAnimation);
            newOpendCard.startAnimation(moveInAnimation);

            // starting new thread using post on click of each image view.
            new Thread(new Runnable() {
                @Override
                public void run() {

                    newOpendCard.post(new Runnable() {
                        @Override
                        public void run() {
                            int imageViewPos = imageViews.lastIndexOf(newOpendCard);
                            final Drawable image = images.get(imageViewPos);
                            final String imageName = imageNameList.get(imageViewPos);

                            newOpendCard.setImageDrawable(image);

                            // when 1st card open
                            if (prevOpendCardPos == -1) {
                                newOpendCard.setClickable(false);
                                prevOpendCardPos = imageViewPos;
                                // when second card open and same card
                            } else if (imageNameList.get(prevOpendCardPos).equals(imageName)) {
                                prevOpendCard = (ImageView) imageAdapter.getItem(prevOpendCardPos);
                                prevOpendCardPos = -1;
                                newOpendCard.setClickable(false);
                                found = true;
                                setScore(correctGuessCountTextView, ++correctGuessCount);
                                // when second card open but different
                            } else {
                                prevOpendCard = (ImageView) imageAdapter.getItem(prevOpendCardPos);
                                prevOpendCard.setClickable(true);
                                prevOpendCardPos = -1;
                                setScore(wrongGuessCountTextView, ++wrongGuessCount);
                            }
                        }
                    });
                }
            }).start();
        }
    };

    /**
     * this method sets the score.
     * @param tv
     * @param score
     */
    private void setScore(TextView tv, int score) {
        tv.setText(score + "");
        if(correctGuessCount == numOfCards/2) {
            int accuracy = (correctGuessCount *100)/(correctGuessCount+wrongGuessCount);
            showDialogBox("Game Over\nAccuracy "+accuracy+"%", R.string.new_game);
        }
    }

    /**
     * shows the dialog boxe
     *
     * @param message
     * @param title
     */

    private void showDialogBox(final String message, final int title) {
        // create a new AlertDialog Builder
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title); // title bar string
        builder.setMessage(message);
        builder.setCancelable(false);
        builder.setPositiveButton(R.string.new_game,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                       setNewGame();
                    } // end method onClick
                } // end anonymous inner class
        ); // end call to setPositiveButton

        // create AlertDialog from the Builder
        AlertDialog resetDialog = builder.create();
        resetDialog.show(); // display the Dialog
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(Menu.NONE, NEW_GAME, Menu.NONE, R.string.new_game);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId())  //gets unique ID of the item
        {
            case NEW_GAME:
                setNewGame();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * this method invokes when to start new game
     *
     * resets all the variables
     */
    private void setNewGame(){
        prevOpendCardPos = -1;
        correctGuessCount = 0;
        wrongGuessCount = 0;
        round = 1;
        found = false;
        linearLayout.setClickable(false);
        setScore(correctGuessCountTextView, correctGuessCount);
        setScore(wrongGuessCountTextView, wrongGuessCount);
        for(ImageView imageView : imageViews) {
            imageView.setImageDrawable(wwe_logo_image);
            imageView.setClickable(true);
        }
        imageAdapter.notifyDataSetChanged();
        setImageLists();
    }
}
